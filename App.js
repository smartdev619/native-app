/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {StackNavigator} from 'react-navigation';
import Home from './src/screens/Home';
import SplashScreen from 'react-native-splash-screen'
const RootStack = StackNavigator(
  {
    Home: {
      screen: Home,
    }
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {

  componentDidMount(){
    SplashScreen.hide();
  }
  render() {
    return (
      <RootStack/>
    );
  }
}

